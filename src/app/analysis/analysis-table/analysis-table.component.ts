import { Component, OnChanges, Input } from '@angular/core';
import { AnalysisService } from '../../service/analysis.service';

@Component({
  selector: 'app-analysis-table',
  templateUrl: './analysis-table.component.html',
  styleUrls: ['./analysis-table.component.scss']
})
export class AnalysisTableComponent implements OnChanges {
  @Input() lastEvent: boolean;
  private displayedColumns:any;
  private dataSource: any;
  private ColumnToDisplay: any;
  private colspan: any;
  private headercolspan: any[];
  constructor(
    private AnalysisService: AnalysisService,
  ) {}

  ngOnChanges() {
    if(this.lastEvent){
      this.getAnalysisAnnualTable();
    }else{
      this.getAnalysisQuarterTable();
    }
  }

  getAnalysisAnnualTable(){
    // this.AnalysisService.getAnalysisHeader('4QG').subscribe((res)=>{
    //   console.log(res);
    // })
    // this.AnalysisService.getAnalysisSubHeader('4QG','analysismetricgroupid').subscribe((res)=>{
    //   console.log(res);
    // })
    // this.AnalysisService.getParameterGroup('4QG').subscribe((res)=>{
    //   console.log(res);
    // })
    // this.AnalysisService.getParameterList('4QG','groupid').subscribe((res)=>{
    //   console.log(res);
    // })
    // this.AnalysisService.getAnalysisValue(7160,'analysismetriccode','paramtergroupid').subscribe((res)=>{
    //   console.log(res);
    // })
    this.displayedColumns = this.AnalysisService.getAnnualDisplayedColumns();
    this.dataSource = this.AnalysisService.getAnnualDataSource();
    this.ColumnToDisplay = ['', 'Units','Latest Financial Annual Report','Preceding Financial Annual Report','Current Year Fiscal Year 4 Quarters','Latest Rolling 4 Quarters','Preceding Rolling  4 Quarter','Current Fiscal Year 4 Quarters VS Latest Financial Annual Report Growth %','Latest VS Preceding Rolling 4 Quarters Growth %','Rolling 4 Quarters VS Latest Financial Annual Report Growth %','Latest Financial Annual Report VS Preceding Financial Annual Report Growth %','Compound Annual Growth Rate 2years','Compound Annual Growth Rate 3years','Compound Annual Growth Rate 4years','Compound Annual Growth Rate 5years'];
    this.colspan = 15;
    this.headercolspan = [2,2,3,4,4];
  }

 getAnalysisQuarterTable(){
  //   this.AnalysisService.getAnalysisHeader('QG').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.AnalysisService.getAnalysisSubHeader('QG','analysismetricgroupid').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.AnalysisService.getParameterGroup('QG').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.AnalysisService.getParameterList('QG','groupid').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.AnalysisService.getAnalysisValue(7160,'analysismetriccode','paramtergroupid').subscribe((res)=>{
  //     console.log(res);
  //   })
    this.displayedColumns = this.AnalysisService.getQuarterDisplayedColumns();
    this.dataSource = this.AnalysisService.getQuarterDataSource();
    this.ColumnToDisplay = ['', 'Units','Latest Quarter','Previous Quarter QOQ','Preceding Quarter YOY',' Growth of Latest VS Previous Quarter ','Growth of Latest VS Preceding Quarter','Latest Cumulative Quarter',' Preceding Cumulative Quarter ','Growth of Latest VS Preceding Cumulative Quarters'];
    this.colspan = 10;
    this.headercolspan = [2,3,2,2,1]
  }

  isGroup(index, item): boolean{
    return item.group;
  }
}
