import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ReportTypeService } from '../service/report-type.service';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss']
})
export class AnalysisComponent implements OnInit {
  toggle$: BehaviorSubject<boolean>;
  lastEvent = true;
  constructor(
    private ReportTypeService: ReportTypeService,
    ){
    this.toggle$ = this.ReportTypeService.getToggle();

    this.toggle$.subscribe(value => {
      this.lastEvent = value;
    });
  } 
  ngOnInit() {
  }

}
