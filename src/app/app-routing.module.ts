import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SnapshotComponent } from './snapshot/snapshot.component';
import { AnalysisComponent } from './analysis/analysis.component';
import { DetailsComponent } from './details/details.component';
import { GraphComponent } from './graph/graph.component';


const routes: Routes = [
  { path: '',   redirectTo: '/snapshot', pathMatch: 'full' },
  { path: 'snapshot', component: SnapshotComponent },
  { path: 'analysis', component: AnalysisComponent },
  { path: 'graph', component: GraphComponent },
  { path: 'details', component: DetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
