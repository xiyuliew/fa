import { Component, OnInit } from '@angular/core';
import { ReportTypeService } from './service/report-type.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  toggle$: BehaviorSubject<boolean>;
  lastEvent = true;
  constructor(
    private ReportTypeService: ReportTypeService,
    ){
    this.toggle$ = this.ReportTypeService.getToggle();

    this.toggle$.subscribe(value => {
      this.lastEvent = value;
    });
  }
  ngOnInit(){}
  changeType1() {
    this.lastEvent = true;
    this.toggle$.next(this.lastEvent);
  }
  changeType2(){
    this.lastEvent = false;
    this.toggle$.next(this.lastEvent);
  }
}


