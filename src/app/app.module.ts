import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { SnapshotComponent } from './snapshot/snapshot.component';
import { AnalysisComponent } from './analysis/analysis.component';
import { DetailsComponent } from './details/details.component';
import { GraphComponent } from './graph/graph.component';
import { DialogGraphComponent } from './dialog/dialog-graph/dialog-graph.component';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { SnapshotTableComponent } from './snapshot/snapshot-table/snapshot-table.component';
import { SnapshotChartComponent } from './snapshot/snapshot-chart/snapshot-chart.component';
import { DetailsTableComponent } from './details/details-table/details-table.component';
import { GraphChartComponent } from './graph/graph-chart/graph-chart.component';
import { GraphTableComponent } from './graph/graph-table/graph-table.component';
import { GraphButtonComponent } from './graph/graph-button/graph-button.component';
import { AnalysisTableComponent } from './analysis/analysis-table/analysis-table.component';
import { MatExpansionModule } from '@angular/material/expansion'

@NgModule({
  declarations: [
    AppComponent,
    SnapshotComponent,
    AnalysisComponent,
    DetailsComponent,
    GraphComponent,
    DialogGraphComponent,
    SnapshotTableComponent,
    SnapshotChartComponent,
    DetailsTableComponent,
    GraphChartComponent,
    GraphTableComponent,
    GraphButtonComponent,
    AnalysisTableComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatButtonToggleModule,
    MatCardModule,
    MatTableModule,
    MatDialogModule,
    HttpClientModule,
    MatExpansionModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[DialogGraphComponent],
})
export class AppModule {
  
 }
