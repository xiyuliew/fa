import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class apiConfig{
    baseURL = 'http://screener-app.southeastasia.cloudapp.azure.com:3000/api/';
    StockTicker = 'stocks/search?term=penta';
    ParameterGroup = 'fa/parameterGroups/reportType/';
    ParameterList = 'fa/parameters/reportType/';
    AnalysisMetricGroup = 'fa/analysisMetricGroups/reportType/';
    AnalysisMetric = 'fa/analysisMetrics/reportType/';
    AnalysisReports = 'fa/analysisReports/';
    AnnualReports = 'fa/annualReports/';
    LatestPrice = 'fa/latest/';
    QuarterlyReports = 'fa/quarterlyReports/';
    TTM = 'fa/ttm/';

    AnalysisMetricGroupID = '?analysisMetricGroupId=';
    ParameterGroupID = '?parameterGroupId=';
    ParameterValue = 'parameterValues?code=';
    AndLatest5Year = '&lastYearNo=5';
    AndLatest12Quarter = '&lastQuarterNo=12';
    AndLatest5Quarter = '&lastQuarterNo=5';
    Latest5Year = '?lastYearNo=5';
    Latest1Year = '?lastYearNo=1';
    Latest1Quarter = '?lastQuarterNo=1';
    Latest12Quarter = '?lastQuarterNo=12';
    data = 'data';


}

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',  
      'Authorization': 'my-auth-token'
    })
  };