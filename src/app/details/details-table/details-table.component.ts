import { Component, OnChanges, Input } from '@angular/core';
import { DetailsService } from '../../service/details.service'

@Component({
  selector: 'app-details-table',
  templateUrl: './details-table.component.html',
  styleUrls: ['./details-table.component.scss']
})
export class DetailsTableComponent implements OnChanges {
  @Input() lastEvent: boolean;
  private displayedColumns:any;
  private stockTicker: any;
  private dataSource: any;
  private ColumnToDisplay: any;
  private colspan: any;
  
  constructor(
    private DetailsService: DetailsService,
  ) {}

  ngOnChanges() {
    if(this.lastEvent){
      this.getDetailsAnnualTable();
    }else{
      this.getDetailsQuarterTable();
    }
  }

  // getDetailsAnnualTable(){
  //   this.DetailsService.getParameterGroup('AR').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.DetailsService.getParameterList('AR','groupdID').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.DetailsService.getAnnualStockTicker1(7160).subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.DetailsService.getAnnualParameterValue(7160,2017,'parameterGroupID').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.DetailsService.getAnnualTTM(7160,'paramterGroupID').subscribe((res)=>{
  //     console.log(res);
  //   })
  //   this.DetailsService.getLastPrice(7160,'parameterGroupID').subscribe((res)=>{
  //     console.log(res);
  //   })

  //   this.displayedColumns = this.DetailsService.getAnnualDisplayedColumns();
  //   this.dataSource = this.DetailsService.getAnnualDetailsTable();
  //   this.ColumnToDisplay = ["","units",'2013 31-Dec','2014 31-Dec','2015 31-Dec','2016 31-Dec','2017 31-Dec',"TTM","LastPrice"]
  // }

  getDetailsAnnualTable(){
    this.displayedColumns = this.DetailsService.getAnnualDisplayedColumns();
    this.dataSource = this.DetailsService.getAnnualDetailsTable();
    this.ColumnToDisplay = ["","units",'31-Dec-2013','31-Dec-2014','31-Dec-2015','31-Dec-2016','31-Dec-2017',"TTM","LastPrice"]
    this.colspan = 9;
  }
  getDetailsQuarterTable(){
    this.displayedColumns = this.DetailsService.getQuarterDisplayedColumns();
    this.dataSource = this.DetailsService.getQuarterDetailsTable();
    this.ColumnToDisplay = ["","units",'31-Dec-2015 Q4','31-Mar-2016 Q1','30-Jun-2016 Q2','30-Sep-2016 Q3','31-Dec-2016 Q4','31-Mar-2017 Q1','30-Jun-2017 Q2','30-Sep-2017 Q3','31-Dec-2017 Q4','31-Mar-2018 Q1','30-Jun-2018 Q2','30-Sep-2018 Q3',"TTM","LastPrice"]
    this.colspan = 16;
  }

  isGroup(index, item): boolean{
    return item.group;
  }
}
