import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ReportTypeService } from '../service/report-type.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  toggle$: BehaviorSubject<boolean>;
  lastEvent = true;
  constructor(
    private ReportTypeService: ReportTypeService,
    ){
    this.toggle$ = this.ReportTypeService.getToggle();

    this.toggle$.subscribe(value => {
      this.lastEvent = value;
    });
  } 
  ngOnInit() {
  }

}
