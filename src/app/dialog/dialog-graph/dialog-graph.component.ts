import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogParameter } from '../../interface/graph';

@Component({
  selector: 'app-dialog-graph',
  templateUrl: './dialog-graph.component.html',
  styleUrls: ['./dialog-graph.component.scss']
})
export class DialogGraphComponent implements OnInit {
    constructor(
      public dialogRef: MatDialogRef<DialogGraphComponent>,
      @Inject(MAT_DIALOG_DATA) public data: DialogParameter,) {}

    ngOnInit(){
    }

    onNoClick(): void {
      this.dialogRef.close();
    }
  }

