import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphButtonComponent } from './graph-button.component';

describe('GraphButtonComponent', () => {
  let component: GraphButtonComponent;
  let fixture: ComponentFixture<GraphButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
