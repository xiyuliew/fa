import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DialogGraphComponent } from '../../dialog/dialog-graph/dialog-graph.component';
import { MatDialog } from '@angular/material';
import { GraphService } from '../../service/graph.service';

@Component({
  selector: 'app-graph-button',
  templateUrl: './graph-button.component.html',
  styleUrls: ['./graph-button.component.scss']
})
export class GraphButtonComponent{
  @Input() lastEvent: boolean;
  @Output() selectedparameter: EventEmitter<any> = new EventEmitter<any>()
  constructor(
    private GraphService: GraphService,
    private dialog: MatDialog,
  ) {}

  openDialog(): any {
    const dialogRef = this.dialog.open(DialogGraphComponent, {
      width:'250px',
      data:{parameter: this.GraphService.getDialogParameter()}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.selectedparameter.emit(result);
    });
  }


}
