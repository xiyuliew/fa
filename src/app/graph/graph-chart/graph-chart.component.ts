import { Component, OnChanges, Input, OnInit, AfterViewInit, ViewChild,ElementRef } from '@angular/core';
import { GraphService } from '../../service/graph.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-graph-chart',
  templateUrl: './graph-chart.component.html',
  styleUrls: ['./graph-chart.component.scss']
})
export class GraphChartComponent implements OnInit {
  GraphChartOpenState = false;
  private _lastEvent: string;
  public get lastEvent(){ return this._lastEvent  }
  @Input() 
  public set lastEvent(newValue:string){
    this._lastEvent = newValue;
    if(this.lastEvent){
      this.chartdata = [];
      this.chartdata = this.GraphService.getAnnualChartData(this.annualdetails);
      this.initGraph();
      this.chartlabel = ['Revenue'];
    }else{
      this.chartdata = [];
      this.chartdata = this.GraphService.getQuarterChartData(this.quarterdetails);
      this.initGraph();
      this.chartlabel = ['Revenue'];
    }
  }

  private _selectedparameter: string;
  public get selectedparameter(){ return this._selectedparameter  }
  @Input() 
  public set selectedparameter(newValue:string){
    if(this._selectedparameter != newValue){
      this._selectedparameter = newValue;
      if(this.lastEvent){
        if(this._selectedparameter != undefined && this.chartdata.datasets.length > 0 && (this.chartlabel.indexOf(this._selectedparameter) == -1)){
          this.addAnnualChartData();
          this.chartlabel.push(this._selectedparameter);
        }
      }else{
        if(this._selectedparameter != undefined && this.chartdata.datasets.length > 0 && (this.chartlabel.indexOf(this._selectedparameter) == -1)){
          this.addQuarterChartData();
          this.chartlabel.push(this._selectedparameter);
        }
      }
    }
  }

  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;
  public chart;
  public chartdata;
  public chartlabel;
  public annualdetails = this.GraphService.getAnnualDetailsTable();
  public quarterdetails = this.GraphService.getQuarterDetailsTable();
  constructor(
    private GraphService: GraphService,
  ) { }
  
  addAnnualChartData(){
    this.chartdata.datasets.push(this.GraphService.addAnnualChart(this.selectedparameter));
    this.chart.update();
  }

  addQuarterChartData(){
    this.chartdata.datasets.push(this.GraphService.addQuarterChart(this.selectedparameter));
    this.chart.update();
  }

  ngOnInit(){
  }

  initGraph(){
    let ctx = this.canvas.nativeElement;
    this.chart = new Chart(ctx, {
      type: 'bar',
      data: this.chartdata,
      options: {
        tooltips: {
          mode: 'label',
        },
        legend:{
          position: 'bottom',
        },
        elements: {
          line: {
            tension: 0,
            fill: false
          }
      },
        scales: {
            xAxes:[{
              gridLines:{
                display:false,
              },            
            }],
            yAxes: [{
              id: 'yaxis_1',
              display: false,
              gridLines:{
                display:false,
                drawBorder: false,
              },ticks: {
                beginAtZero: true,  
            },
            },{
                id: 'yaxis_2',
                display: false,
                gridLines:{
                  display:false,
                  drawBorder: false,
            },ticks: {
              beginAtZero: true,  
          },
          },{
              id: 'yaxis_3',
              display: false,
              gridLines:{
                display:false,
                drawBorder: false,
              },
              ticks: {
                  beginAtZero: true,  
              },
            },{
              id: 'yaxis_4',
              display: false,
              gridLines:{
                display:false,
                drawBorder: false,
              },
              ticks: {
                  beginAtZero: true,  
              },
            }],
          },
      },
    });
  }
}
