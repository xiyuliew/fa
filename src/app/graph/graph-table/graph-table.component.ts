import { Component, OnInit,Input, OnChanges, SimpleChanges } from '@angular/core';
import { GraphService } from '../../service/graph.service';
import { AnnualGraphTableInterface, QuarterGraphTableInterface } from '../../interface/graph';
import { MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-graph-table',
  templateUrl: './graph-table.component.html',
  styleUrls: ['./graph-table.component.scss']
}) 
export class GraphTableComponent implements OnInit {
  GraphTableOpenState = false;
  private _selectedparameter: string;
  public get selectedparameter(){ return this._selectedparameter  }
  @Input() 
  public set selectedparameter(newValue:string){
    if(this._selectedparameter != newValue){
      this._selectedparameter = newValue;
      console.log(this._selectedparameter);
      if(this.lastEvent){
        if(this._selectedparameter!= undefined && this.dataSource.data.length > 0 && !(this.dataSource.data.some(x => x.parameter === this._selectedparameter))){
          this.GraphService.addAnnualTableRow(this._selectedparameter,this.dataSource);
        }
      }else{
        if(this._selectedparameter!= undefined && this.dataSource2.data.length > 0 && !(this.dataSource2.data.some(x => x.parameter === this._selectedparameter))){
          this.GraphService.addQuarterTableRow(this._selectedparameter,this.dataSource2);
      }
    }
    }
  }
  @Input() lastEvent: boolean;
  private details: any[];
  private TableData;
  private ColumnToDisplay: any[];
  private displayedColumns: any[];
  public dataSource: MatTableDataSource<AnnualGraphTableInterface>;
  public dataSource2: MatTableDataSource<QuarterGraphTableInterface>;
  constructor(
    private GraphService: GraphService,
  ) {
    let annualdetailsInterface: AnnualGraphTableInterface[] = [];
    let quarterdetailsInterface: QuarterGraphTableInterface[] = [];
    this.dataSource = new MatTableDataSource(annualdetailsInterface);
    this.dataSource2 = new MatTableDataSource(quarterdetailsInterface);
  }

  ngOnInit() {
    if(this.lastEvent){
      this.TableData = this.dataSource;
      this.details = this.GraphService.getAnnualDetailsTable();
      this.dataSource.data.push(this.details[0]);
      this.ColumnToDisplay = ["","units",'31-Dec-2013','31-Dec-2014','31-Dec-2015','31-Dec-2016','31-Dec-2017',"TTM","LastPrice"]
      this.displayedColumns = this.GraphService.getAnnualDisplayedColumns();
    }else{
      this.TableData = this.dataSource2;
      this.details = this.GraphService.getQuarterDetailsTable();
      this.dataSource2.data.push(this.details[0]);
      this.ColumnToDisplay = ["","units",'31-Dec-2015 Q4','31-Mar-2016 Q1','30-Jun-2016 Q2','30-Sep-2016 Q3','31-Dec-2016 Q4','31-Mar-2017 Q1','30-Jun-2017 Q2','30-Sep-2017 Q3','31-Dec-2017 Q4','31-Mar-2018 Q1','30-Jun-2018 Q2','30-Sep-2018 Q3',"TTM","LastPrice"]
      this.displayedColumns = this.GraphService.getQuarterDisplayedColumns();
    }
  }

}
