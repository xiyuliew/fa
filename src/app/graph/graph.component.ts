import { Component, OnInit, OnChanges} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ReportTypeService } from '../service/report-type.service';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent{
  toggle$: BehaviorSubject<boolean>;
  lastEvent:any;
  dialog_parameter:any;
  constructor(
    private ReportTypeService: ReportTypeService,
    ){
    this.toggle$ = this.ReportTypeService.getToggle();
    this.toggle$.subscribe(value => {
      this.lastEvent = value;
      this.dialog_parameter = undefined;
    });
  }   

  selectedparameter(result: string) {
    this.dialog_parameter = result;
  }
}

