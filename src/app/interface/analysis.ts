export interface GroupParameter{
  group: string;
}
export interface AnnualAnalysis {
    parameter: string;
    units: string;
    LatestAFR: string;
    PrecedingAFR: string;
    Current4Q: string;
    LatestRolling4Q: string;
    PrecedingRolling4Q: string;
    Current4QAndLatestAFR: string;
    LatestAndPreceding4QGrowth: string;
    Rolling4QAndLatestAFR: string;
    LatestAFRAndPrecedingAFR: string;
    CAGR2: string;
    CAGR3: string;
    CAGR4: string;
    CAGR5: string;
  }

  
export interface QuarterAnalysis {
    parameter: string;
    units: string;
    LatestQuarter: string;
    PreviousQuarter: string;
    PrecedingQuarter: string;
    LatestAndPreviousQuarter: string;
    LatestAndPrecedingQuarter: string;
    LatestCumulativeQuarter: string;
    PrecedingCumulativeQuarter: string;
    CumulativeGrowth: string;
  }

  
export interface ParameterGroup {
  id: number;
  name: string;
}

export interface ParameterList{
  code: string;
  abbrv: string;
  full_name: string;
  unit: string;
}

export interface AnalysisHeader{
  id: number;
  name: string;
}

export interface AnalysisSubHeader{
  code: string;
  abbrv: string;
  name: string;
}
export interface AnalysisValue{
  code: string;
  fmt_value: string;
}
