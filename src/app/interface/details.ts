export interface GroupParameter{
    group: string;
  }
  
export interface AnnualDetailsInterface {
    parameter: string;
    units: string;
    Annual1: string;
    Annual2: string;
    Annual3: string;
    Annual4: string;
    Annual5: string;
    TTM: string;
    LastPrice: string;
  };
  
export interface AnnualDetailsParameterGroups {
    code: number;
    parameter: string;
    units: string; 
};

export interface AnnualDetailsParameterValues{
    code: number;
    Annual1: number;
    Annual2: number;
    Annual3: number;
    Annual4: number;
    Annual5: number;
    TTM: number;
    LastPrice: number;
};

export interface QuarterDetailsParameterGroups{
    code: number;
    parameter: string;
    units: string;
}

export interface QuarterDetailsParameterValues{
    code: number;
    Quarter1: number;
    Quarter2: number;
    Quarter3: number;
    Quarter4: number;
    Quarter5: number;
    Quarter6: number;
    Quarter7: number;
    Quarter8: number;
    Quarter9: number;
    Quarter10: number;
    Quarter11: number;
    Quarter12: number;
    TTM: number;
    LastPrice: number;
}
export interface QuarterDetailsInterface {
    parameter: string;
    units: string;
    Quarter1: string;
    Quarter2: string;
    Quarter3: string;
    Quarter4: string;
    Quarter5: string;
    Quarter6: string;
    Quarter7: string;
    Quarter8: string;
    Quarter9: string;
    Quarter10: string;
    Quarter11: string;
    Quarter12: string;
    TTM: string;
    LastPrice: string;
}

//


export interface ParameterGroup {
    id: number;
    name: string;
  }

export interface ParameterList{
    code: string;
    abbrv: string;
    full_name: string;
    unit: string;
}

export interface AnnualStockTicker{
    stock_ticker: string;
    fiscal_year: number;
    fye_date: string;
    announcement_date: string;
}

export interface QuarterlyStockTicker{
    stock_ticker: string;
    fiscal_year: number;
    quarter_id: number;
    quarter_abbrv_: string;
    quarter_date: string;
    announcement_date: string;
}

export interface AnnualParameterValue{
    code: string;
    fmt_value: string;
}

export interface AnnualTTM{
    code: string;
    fmt_value: string;
}

export interface LastPrice{
    code: string;
    fmt_value: string;
}

export interface QuarterParameterValue{
    code:string;
    fmt_value: string;
}