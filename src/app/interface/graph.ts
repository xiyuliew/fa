export interface DialogParameter {
    parameter: string;
  }

    
export interface AnnualGraphTableInterface {
  parameter: string;
  units: string;
  Annual1: number;
  Annual2: number;
  Annual3: number;
  Annual4: number;
  Annual5: number;
  TTM: number;
  LastPrice: number;
};

export interface QuarterGraphTableInterface {
  parameter: string;
  units: string;
  Quarter1: number;
  Quarter2: number;
  Quarter3: number;
  Quarter4: number;
  Quarter5: number;
  Quarter6: number;
  Quarter7: number;
  Quarter8: number;
  Quarter9: number;
  Quarter10: number;
  Quarter11: number;
  Quarter12: number;
  TTM: number;
  LastPrice: number;
}




export interface ParameterList{
  code: string;
  abbrv: string;
  full_name: string;
  unit: string;
}

export interface AnnualStockTicker{
  stock_ticker: string;
  fiscal_year: number;
  fye_date: string;
  announcement_date: string;
}

export interface QuarterlyStockTicker{
  stock_ticker: string;
  fiscal_year: number;
  quarter_id: number;
  quarter_abbrv_: string;
  quarter_date: string;
  announcement_date: string;
}

export interface AnnualParameterValue{
  code: string;
  fmt_value: string;
}

export interface AnnualTTM{
  code: string;
  fmt_value: string;
}

export interface QuarterParameterValue{
  code:string;
  fmt_value: string;
}