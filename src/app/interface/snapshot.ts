export interface SnapshotTableParameter {
    parameter: string;
    value: any;
  }

export interface ParameterGroup {
  id: number;
  name: string;
}

export interface ParameterList{
  code: string;
  abbrv: string;
  full_name: string;
  unit: string;
}

export interface AnnualLatestYear{
  stock_ticker: string;
  fiscal_year: number;
  fye_date: string;
  announcement_date: string;
}

export interface QuarterLatestYear{
  stock_ticker: string;
  fiscal_year: number;
  quarter_id: number;
  quarter_abbrv: string;
  quarter_date: string;
}

export interface ParameterValue{
  code: string;
  fmt_value: string;
}

export interface AnnualChartData{
  fiscal_year: number;
  value: number;
  fmt_value: string
}

export interface QuarterChartData{
  fiscal_year: number;
  quarter_id: number;
  value: number;
  fmt_value: string
}

