import { AnnualDetailsInterface, AnnualDetailsParameterGroups, AnnualDetailsParameterValues, GroupParameter } from './interface/details';
import { QuarterDetailsInterface, QuarterDetailsParameterGroups,QuarterDetailsParameterValues } from './interface/details';

export const SampleAnnualDetails: (AnnualDetailsInterface|GroupParameter)[] = [
    {group: 'Profitability Value and Growth'},
    {parameter: 'Revenue', units: "RM'000", Annual1:'67,344',Annual2:'81,047',Annual3:'83,604',Annual4:'151,938',Annual5:'284,190',TTM:'408,516',LastPrice:'108,333'},
    {parameter: 'Revenue Growth', units: "%", Annual1:'0.0%',Annual2:'20.3%',Annual3:'3.2%',Annual4:'81.7%',Annual5:'87.0%',TTM:'43.7%',LastPrice:'5.9%'},
    {parameter: 'Gross Profit', units: "RM'000", Annual1:'11,875',Annual2:'21,238',Annual3:'24,315',Annual4:'48,469',Annual5:'80,637',TTM:'129,373',LastPrice:'36,440'},
    {parameter: 'Gross Profit Growth', units: "%", Annual1:'0.0%',Annual2:'7.8%',Annual3:'14.5%',Annual4:'99.3%',Annual5:'66.4%',TTM:'60.4%',LastPrice:'6.9%'},
    {parameter: 'Gross Profit Margin', units: "%", Annual1:'17.6%',Annual2:'26.2%',Annual3:'29.1%',Annual4:'31.9%',Annual5:'28.4%',TTM:'31.7%',LastPrice:'33.6%'},
    {parameter: 'EBITDA', units: "RM'000", Annual1:'105,92',Annual2:'11,537',Annual3:'19,128',Annual4:'3,7288',Annual5:'49,469',TTM:'95,482',LastPrice:'32,616'},
    {parameter: 'EBITDA Growth', units: "%", Annual1:'0.0%',Annual2:'8.9%',Annual3:'65.8%',Annual4:'94.9%',Annual5:'32.7%',TTM:'93.0%',LastPrice:'21.7%'},
    {parameter: 'EBITDA Margin', units: "%", Annual1:'15.7%',Annual2:'14.2%',Annual3:'22.9%',Annual4:'24.5%',Annual5:'17.4%',TTM:'23.4%',LastPrice:'30.1%'},
    {parameter: 'Operating Profit', units: "RM'000", Annual1:'4,276',Annual2:'7,610',Annual3:'14,692',Annual4:'28,932',Annual5:'44,186',TTM:'82,852',LastPrice:'28,613'},
    {parameter: 'Operating Profit Growth', units: "%", Annual1:'0.0%',Annual2:'77.9%',Annual3:'93.1%',Annual4:'96.9%',Annual5:'52.7%',TTM:'87.5%',LastPrice:'18.3%'},
    {parameter: 'Operating Profit Margin', units: "%",Annual1:'6.4%',Annual2:'9.4%',Annual3:'17.6%',Annual4:'19.0%',Annual5:'15.5%',TTM:'20.3%',LastPrice:'26.4%'},
]

export const SampleAnnualParameterGroups: AnnualDetailsParameterGroups[]=[
    {code:1, parameter: 'Revenue', units: "RM'000" },
    {code:2, parameter: 'Revenue Growth', units: "%"},
    {code:3, parameter: 'Gross Profit', units: "RM'000"},
    {code:4, parameter: 'Gross Profit Growth', units: "%"},
    {code:5, parameter: 'Gross Profit Margin', units: "%"},
    {code:6, parameter: 'EBITDA', units: "RM'000"},
    {code:7, parameter: 'EBITDA Growth', units: "%"},
    {code:8, parameter: 'EBITDA Margin', units: "%"},
    {code:9, parameter: 'Operating Profit', units: "RM'000"},
    {code:10,parameter: 'Operating Profit Growth', units: "%"},
    {code:11,parameter: 'Operating Profit Margin', units: "%"}

];

export const SampleAnnualParameterValues: AnnualDetailsParameterValues[]=[
    {code:1, Annual1:67344,Annual2:81047,Annual3:83604,Annual4:151938,Annual5:284190,TTM:408516,LastPrice:108333},
    {code:2, Annual1:0.0,Annual2:20.3,Annual3:3.2,Annual4:81.7,Annual5:87.0,TTM:43.7,LastPrice:5.9},
    {code:3, Annual1:11875,Annual2:21238,Annual3:24315,Annual4:48469,Annual5:80637,TTM:129373,LastPrice:36440},
    {code:4, Annual1:0.0,Annual2:7.8,Annual3:14.5,Annual4:99.3,Annual5:66.4,TTM:60.4,LastPrice:6.9},
    {code:5, Annual1:17.6,Annual2:26.2,Annual3:29.1,Annual4:31.9,Annual5:28.4,TTM:31.7,LastPrice:33.6},
    {code:6, Annual1:10592,Annual2:11537,Annual3:19128,Annual4:37288,Annual5:49469,TTM:95482,LastPrice:32616},
    {code:7, Annual1:0.0,Annual2:8.9,Annual3:65.8,Annual4:94.9,Annual5:32.7,TTM:93.0,LastPrice:21.7},
    {code:8, Annual1:15.7,Annual2:14.2,Annual3:22.9,Annual4:24.5,Annual5:17.4,TTM:23.4,LastPrice:30.1},
    {code:9, Annual1:4276,Annual2:7610,Annual3:14692,Annual4:28932,Annual5:44186,TTM:82852,LastPrice:28613},
    {code:10, Annual1:0.0,Annual2:77.9,Annual3:93.1,Annual4:96.9,Annual5:52.7,TTM:87.5,LastPrice:18.3},
    {code:11,Annual1:6.4,Annual2:9.4,Annual3:17.6,Annual4:19.0,Annual5:15.5,TTM:20.3,LastPrice:26.4}
];

export const SampleQuarterParameterGroups: QuarterDetailsParameterGroups[]=[
    {code:1,parameter: 'Revenue', units: "RM'000"},
];

export const SampleQuarterParameterValues: QuarterDetailsParameterValues[]=[
    {code:1, Quarter1:19256,Quarter2:28599,Quarter3:38848,Quarter4:40684,Quarter5:43476,Quarter6:47572,Quarter7:54811,Quarter8:83302,Quarter9:98504,Quarter10:99383,Quarter11:102296,Quarter12:108333,TTM:408516,LastPrice:108333},
];

export const SampleQuarterDetails: (QuarterDetailsInterface|GroupParameter)[] = [
    {group: 'Profitability Value and Growth'},
    {parameter: 'Revenue', units: "RM'000",Quarter1:'19,256',Quarter2:'28,599',Quarter3:'38,848',Quarter4:'40,684',Quarter5:'43,476',Quarter6:'47,572',Quarter7:'54,811',Quarter8:'83,302',Quarter9:'98,504',Quarter10:'99,383',Quarter11:'102,296',Quarter12:'108,333',TTM:'408,516',LastPrice:'108,333'},
    {parameter: 'Revenue Growth', units: "%",Quarter1:'0.0%',Quarter2:'48.5%',Quarter3:'35.8%',Quarter4:'4.7%',Quarter5:'6.9%',Quarter6:'9.4%',Quarter7:'15.2%',Quarter8:'52.0%',Quarter9:'18.2%',Quarter10:'0.9%',Quarter11:'2.9%',Quarter12:'5.9%',TTM:'43.7%',LastPrice:'5.9%'},
    {parameter: 'Gross Profit', units: "RM'000",Quarter1:'7,223',Quarter2:'8,575',Quarter3:'12,258',Quarter4:'12,156',Quarter5:'15,163',Quarter6:'13,105',Quarter7:'17,596',Quarter8:'20,680',Quarter9:'29,345',Quarter10:'29497',Quarter11:'34,,091',Quarter12:'36,440',TTM:'129,373',LastPrice:'36,440'},
    {parameter: 'Gross Profit Growth', units: "%",Quarter1:'0.0%',Quarter2:'18.7%',Quarter3:'43.0%',Quarter4:'-0.8%',Quarter5:'24.7%',Quarter6:'-14.2%',Quarter7:'35.2%',Quarter8:'17.5%',Quarter9:'41.9%',Quarter10:'0.5%',Quarter11:'15.6%',Quarter12:'6.9%',TTM:'60.4%',LastPrice:'6.9%'},
    {parameter: 'Gross Profit Margin', units: "%",Quarter1:'37.5%',Quarter2:'30.0%',Quarter3:'31.6%',Quarter4:'29.9%',Quarter5:'34.9%',Quarter6:'27.4%',Quarter7:'32.1%',Quarter8:'24.8%',Quarter9:'29.8%',Quarter10:'29.7%',Quarter11:'33.3%',Quarter12:'33.6%',TTM:'31.7%',LastPrice:'33.6%'},
    {parameter: 'EBITDA', units: "RM'000",Quarter1:'6,850',Quarter2:'5,160',Quarter3:'11,463',Quarter4:'12,341',Quarter5:'15,721',Quarter6:'10,253',Quarter7:'13,596',Quarter8:'12,038',Quarter9:'18,772',Quarter10:'17,296',Quarter11:'26,789',Quarter12:'32,616',TTM:'95,482',LastPrice:'32,616'},
    {parameter: 'EBITDA Growth', units: "%",Quarter1:'0.0%',Quarter2:'-24.7%',Quarter3:'122.2%',Quarter4:'7.7%',Quarter5:'27.4%',Quarter6:'-34.8%',Quarter7:'32.6%',Quarter8:'-11.5%',Quarter9:'55.9%',Quarter10:'-7.9%',Quarter11:'55.9%',Quarter12:'54.9%',TTM:'93.0%',LastPrice:'54.9%'},
    {parameter: 'EBITDA Margin', units: "%",Quarter1:'35.6%',Quarter2:'18.0%',Quarter3:'29.5%',Quarter4:'30.3%',Quarter5:'36.2%',Quarter6:'21.6%',Quarter7:'24.8%',Quarter8:'14.5%',Quarter9:'19.1%',Quarter10:'17.4%',Quarter11:'26.2%',Quarter12:'30.1%',TTM:'23.4%',LastPrice:'30.1%'},
    {parameter: 'Operating Profit', units: "RM'000",Quarter1:'2,414',Quarter2:'4,240',Quarter3:'8,579',Quarter4:'8,416',Quarter5:'7,365',Quarter6:'9,365',Quarter7:'11,807',Quarter8:'9,525',Quarter9:'13,489',Quarter10:'16,556',Quarter11:'24,194',Quarter12:'28,613',TTM:'82,852',LastPrice:'28,613'},
    {parameter: 'Operating Profit Growth', units: "%",Quarter1:'19,256',Quarter2:'28,599',Quarter3:'38,848',Quarter4:'40,684',Quarter5:'43,476',Quarter6:'47,572',Quarter7:'54,811',Quarter8:'83,302',Quarter9:'98,504',Quarter10:'99,383',Quarter11:'102,296',Quarter12:'108,333',TTM:'408,516',LastPrice:'108,333'},
    // {parameter: 'Operating Profit Margin', units: "%",Quarter1:19256,Quarter2:28599,Quarter3:38848,Quarter4:40684,Quarter5:43476,Quarter6:47572,Quarter7:54811,Quarter8:83302,Quarter9:98504,Quarter10:99383,Quarter11:102296,Quarter12:108333,TTM:408516,LastPrice:108333},
    // {parameter: 'Interest Expenses', units: "RM'000",Quarter1:19256,Quarter2:28599,Quarter3:38848,Quarter4:40684,Quarter5:43476,Quarter6:47572,Quarter7:54811,Quarter8:83302,Quarter9:98504,Quarter10:99383,Quarter11:102296,Quarter12:108333,TTM:408516,LastPrice:108333},
    // {parameter: 'Profit before tax', units: "RM'000",Quarter1:19256,Quarter2:28599,Quarter3:38848,Quarter4:40684,Quarter5:43476,Quarter6:47572,Quarter7:54811,Quarter8:83302,Quarter9:98504,Quarter10:99383,Quarter11:102296,Quarter12:108333,TTM:408516,LastPrice:108333},
    // {parameter: 'Profit before tax growth', units: "%",Quarter1:19256,Quarter2:28599,Quarter3:38848,Quarter4:40684,Quarter5:43476,Quarter6:47572,Quarter7:54811,Quarter8:83302,Quarter9:98504,Quarter10:99383,Quarter11:102296,Quarter12:108333,TTM:408516,LastPrice:108333},
]