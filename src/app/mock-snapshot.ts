import { SnapshotTableParameter } from './interface/snapshot';


export const SampleAnnualTable1: SnapshotTableParameter[] = [
    {parameter:"Revenue(RM'000)" , value:284190},
    {parameter:"Gross Profit(RM'000)",value:80637},
    {parameter:"Profit Before Tax(RM'000)",value: 43981},
    {parameter:"Net Profit(RM'000)",value:39172},
  ];

export const SampleAnnualTable2: SnapshotTableParameter[] = [
    {parameter:"Net Cash flow From Operation(RM'000)" , value:34097},
    {parameter:"Net Cash Increase/Decrease(RM'000)",value:54190},
    {parameter:"Cash & Cash Equivalent at the End of Period(RM'000)",value: 82202},
    {parameter:"Free Cash Flow(RM'000)",value:28539},
  ];
   
export const SampleAnnualTable3: SnapshotTableParameter[] = [
  {parameter:"Current Assets(RM'000)" , value:306255},
  {parameter:"Total Assets(RM'000)",value:356249},
  {parameter:"Current Liabilities(RM'000)",value: 151772},
  {parameter:"Total Liabilities(RM'000)",value:155903},
];

export const SampleAnnualTable4: SnapshotTableParameter[] = [
  {parameter:"Earning Per Share(Cents)[EPS]" , value:11.36},
  {parameter:"Dividend Per Share(Cents)[DPS]",value:8.00},
  {parameter:"Cash Flow Per Share(RM)[CFPS]",value: 0.108},
  {parameter:"Net Assets Per Share(RM)[NAPS]",value:0.633},
];

export const SampleAnnualTable5: SnapshotTableParameter[] = [
  {parameter:"Revenue Growth" , value:'87.0%'},
  {parameter:"Gross Profit Growth",value:'66.4%'},
  {parameter:"Gross Profit Margin",value: '28.4%'},
  {parameter:"EBITDA Growth",value:'32.7%'},
  {parameter:"EBITDA Margin",value:'17.4%'},
  {parameter:"EBIT Growth",value:'52.7%'},
  {parameter:"EBIT Margin",value:'15.5%'},
  {parameter:"Profit Before Tax Growth",value:'52.5%'},
  {parameter:"Profit Before Tax Margin",value:'15.5%'},
  {parameter:"Net Profit Growth",value:'32.4%'},
  {parameter:"Net Profit Margin",value:'13.8%'},
  {parameter:"Earning Per Share Growth Rate",value:'-39.4%'},
  {parameter:"Net Cash Flow from Operation Growth",value:'95.2%'},
  {parameter:"Cash & Cash Equivalent at the End of Period Growth",value:'166.5%'},
  {parameter:"Cash Per Share Growth",value:'166.5%'},
  {parameter:"Cash Flow Per Share Growth",value:'197.9%'},
  {parameter:"Free Cash Flow Per Share Growth",value:'116.6%'},
  {parameter:"Enterprise Value Growth",value:'407.1%'},
  {parameter:"Return on Assets",value:'11.0%'},
  {parameter:"Return on Average Assets",value:'15.68%'},
  {parameter:"Return on Equity ",value:'19.55%'},
  {parameter:"Return on Average Equity",value:'25.07%'},
  {parameter:"Assets Turnover",value:'79.77%'},
  {parameter:"Average Assets Turnover",value:'113.74%'},
];
export const SampleAnnualTable6: SnapshotTableParameter[] = [
  {parameter:"Net tangible sset per share" , value:0.623},
  {parameter:"Price to book ratio",value:4.79},
  {parameter:"Price to earning per share ratio",value: -67.62},
  {parameter:"Dividend per out ratio",value:'70.4%'},
  {parameter:"Dividend yeild",value:'2.6%'},
  {parameter:"Cash per share",value:0.26},
  {parameter:"Cash flow per share",value:0.108},
  {parameter:"Free cash flow per share",value:0.090},
  {parameter:"EV/EBITDA ratio",value:21.29},
  {parameter:"EV/EBIT ratio",value:23.83},
]

export const SampleAnnualTable7: SnapshotTableParameter[] = [
  {parameter:"Acid test / quick ratio" , value:1.217},
  {parameter:"Current ratio",value:2.019},
  {parameter:"Debt to equity ratio",value: 0.778},
  {parameter:"Debt ratio",value:0.438},
  {parameter:"Net debt ratio",value:0.012},
  {parameter:"Long term debt ratio",value:0.011},
  {parameter:"Net gearing ratio",value:0.368},
  {parameter:"Equity ratio",value:0.562},
];

export const SampleAnnualTable8: SnapshotTableParameter[] = [
  {parameter:"Piotroski's F-Score 9-point" , value:3},
  {parameter:"Altman's Z-Score 5-point (Mfrg)",value:5.82},
  {parameter:"Altman's Z-Score 5-point (Non-Mfrg)",value: 11.06},
  {parameter:"Altman's Z-Score 5-point (EM)",value:16.93},
]

export const SampleQuarterTable1: SnapshotTableParameter[] = [
  {parameter:"Revenue(RM'000)" , value:106333},
  {parameter:"Gross Profit(RM'000)",value:36440},
  {parameter:"Profit Before Tax(RM'000)",value: 28556},
  {parameter:"Net Profit(RM'000)",value:26717},
];

export const SampleQuarterTable2: SnapshotTableParameter[] = [
  {parameter:"Net Cash flow From Operation(RM'000)" , value:66981},
  {parameter:"Net Cash Increase/Decrease(RM'000)",value:238613},
  {parameter:"Cash & Cash Equivalent at the End of Period(RM'000)",value: 326104},
  {parameter:"Free Cash Flow(RM'000)",value:29123},
];

export const SampleQuarterTable3: SnapshotTableParameter[] = [
  {parameter:"Current Assets(RM'000)" , value:555476},
  {parameter:"Total Assets(RM'000)",value:640679},
  {parameter:"Current Liabilities(RM'000)",value: 191781},
  {parameter:"Total Liabilities(RM'000)",value:195540},
];

export const SampleQuarterTable4: SnapshotTableParameter[] = [
  {parameter:"Earning Per Share(Cents)[EPS]" , value:5.40},
  {parameter:"Dividend Per Share(Cents)[DPS]",value:3.00},
  {parameter:"Cash Flow Per Share(RM)[CFPS]",value: 0.212},
  {parameter:"Net Assets Per Share(RM)[NAPS]",value:1.406},
];

export const SampleQuarterTable5: SnapshotTableParameter[] = [
  {parameter:"Revenue Growth" , value:'5.9%'},
  {parameter:"Gross Profit Growth",value:'6.9%'},
  {parameter:"Gross Profit Margin",value: '33.6%'},
  {parameter:"EBITDA Growth",value:'21.7%'},
  {parameter:"EBITDA Margin",value:'30.1%'},
  {parameter:"EBIT Growth",value:'18.3%'},
  {parameter:"EBIT Margin",value:'26.4%'},
  {parameter:"Profit Before Tax Growth",value:'18.4%'},
  {parameter:"Profit Before Tax Margin",value:'26.4%'},
  {parameter:"Net Profit Growth",value:'16.5%'},
  {parameter:"Net Profit Margin",value:'24.7%'},
  {parameter:"Earning Per Share Growth Rate",value:'22.45%'},
  {parameter:"Net Cash Flow from Operation Growth",value:'293.6%'},
  {parameter:"Cash & Cash Equivalent at the End of Period Growth",value:'9.3%'},
  {parameter:"Cash Per Share Growth",value:'9.3%'},
  {parameter:"Cash Flow Per Share Growth",value:'293.6%'},
  {parameter:"Free Cash Flow Per Share Growth",value:'1599.1%'},
  {parameter:"Enterprise Value Growth",value:'-'},
  {parameter:"Return on Assets",value:'4.17%'},
  {parameter:"Return on Average Assets",value:'4.29%'},
  {parameter:"Return on Equity ",value:'6.00%'},
  {parameter:"Return on Average Equity",value:'6.19%'},
  {parameter:"Assets Turnover",value:'16,91%'},
  {parameter:"Average Assets Turnover",value:'17.39%'},
];

export const SampleQuarterTable6: SnapshotTableParameter[] = [
  {parameter:"Net tangible sset per share" , value:1.371},
  {parameter:"Price to book ratio",value:2.63},
  {parameter:"Price to earning per share ratio",value: 305.22},
  {parameter:"Dividend per out ratio",value:'-'},
  {parameter:"Dividend yeild",value:'-'},
  {parameter:"Cash per share",value:1.030},
  {parameter:"Cash flow per share",value:0.212},
  {parameter:"Free cash flow per share",value:0.092},
  {parameter:"EV/EBITDA ratio",value:'-'},
  {parameter:"EV/EBIT ratio",value:'-'},
]


export const SampleQuarterTable7: SnapshotTableParameter[] = [
  {parameter:"Acid test / quick ratio" , value:2.239},
  {parameter:"Current ratio",value:2.896},
  {parameter:"Debt to equity ratio",value: 0.439},
  {parameter:"Debt ratio",value:0.305},
  {parameter:"Net debt ratio",value:0.006},
  {parameter:"Long term debt ratio",value:0.005},
  {parameter:"Net gearing ratio",value:'0.0293'},
  {parameter:"Equity ratio",value:0.695},
]

export const SampleQuarterTable8: SnapshotTableParameter[] = [
  {parameter:"Piotroski's F-Score 9-point" , value:8.00},
  {parameter:"Altman's Z-Score 5-point (Mfrg)",value:5.16},
  {parameter:"Altman's Z-Score 5-point (Non-Mfrg)",value: 11.63},
  {parameter:"Altman's Z-Score 5-point (EM)",value:17.23},
]

