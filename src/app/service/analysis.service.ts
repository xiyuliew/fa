import { Injectable } from '@angular/core';
import { SampleAnnualAnalysis, SampleQuarterAnalysis } from '../mock-analysis';
import { AnnualAnalysis, QuarterAnalysis, GroupParameter} from '../interface/analysis';
import { ParameterGroup, ParameterList, AnalysisHeader, AnalysisSubHeader, AnalysisValue} from '../interface/analysis';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { apiConfig } from '../constant';

@Injectable({
  providedIn: 'root'
})
export class AnalysisService {
  constructor(
    private api: apiConfig,
    private http: HttpClient,
  ) { }

  getParameterGroup(code: any): Observable<ParameterGroup[]>{
    var url = `${this.api.baseURL}` + `${this.api.ParameterGroup}` + code;
    return this.http.get<ParameterGroup[]>(url).pipe();
  }

  getParameterList(code:any, groupID: any): Observable<ParameterList[]>{
    var url = `${this.api.baseURL}` + `${this.api.ParameterList}` + code +`${this.api.ParameterGroupID}` + groupID;
    return this.http.get<ParameterList[]>(url).pipe();
  }

  getAnalysisHeader(code: any):Observable<AnalysisHeader[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnalysisMetricGroup}` + code;
    return this.http.get<AnalysisHeader[]>(url).pipe();
  }

  getAnalysisSubHeader(code: any, analysisMetricGroupID) :Observable<AnalysisSubHeader[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnalysisMetricGroup}` + code + `${this.api.AnalysisMetricGroupID}` + analysisMetricGroupID;
    return this.http.get<AnalysisSubHeader[]>(url).pipe();
  }

  getAnalysisValue(stockTicker:any, analysisMetricCode: any, parameterGroupID: any):Observable<AnalysisValue[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnalysisReports}` + stockTicker + analysisMetricCode  + `${this.api.data}` +`${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<AnalysisValue[]>(url).pipe();
  }



  getAnnualDisplayedColumns(){
    return ['parameter', 'units','LatestAFR','PrecedingAFR','Current4Q','LatestRolling4Q','PrecedingRolling4Q','Current4QAndLatestAFR','LatestAndPreceding4QGrowth','Rolling4QAndLatestAFR','LatestAFRAndPrecedingAFR','CAGR2','CAGR3','CAGR4','CAGR5'];
  }
  getAnnualDataSource(): (AnnualAnalysis|GroupParameter)[]{
    return SampleAnnualAnalysis;
  }
  getQuarterDisplayedColumns(){
    return ['parameter', 'units','LatestQuarter','PreviousQuarter','PrecedingQuarter','LatestAndPreviousQuarter','LatestAndPrecedingQuarter','LatestCumulativeQuarter','PrecedingCumulativeQuarter','CumulativeGrowth'];
  }
  getQuarterDataSource(): (QuarterAnalysis|GroupParameter)[]{
    return SampleQuarterAnalysis;
  }
}
