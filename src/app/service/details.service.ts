import { Injectable } from '@angular/core';
import { SampleAnnualDetails, SampleAnnualParameterGroups,SampleAnnualParameterValues, SampleQuarterParameterValues, SampleQuarterParameterGroups, SampleQuarterDetails } from '../mock-details';
import { AnnualDetailsInterface,AnnualDetailsParameterGroups,AnnualDetailsParameterValues, GroupParameter, QuarterDetailsInterface, QuarterDetailsParameterGroups,QuarterDetailsParameterValues } from '../interface/details';
import { ParameterGroup, ParameterList, AnnualStockTicker, QuarterlyStockTicker, AnnualParameterValue,AnnualTTM,LastPrice, QuarterParameterValue} from '../interface/details';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { apiConfig } from '../constant';

@Injectable({
  providedIn: 'root'
})
export class DetailsService {
  constructor(
    private api: apiConfig,
    private http: HttpClient,
  ) { }
  
  getParameterGroup(code: any): Observable<ParameterGroup[]>{
    var url = `${this.api.baseURL}` + `${this.api.ParameterGroup}` + code;
    return this.http.get<ParameterGroup[]>(url).pipe();
  } 

  getParameterList(code:any, groupID: any): Observable<ParameterList[]>{
    var url = `${this.api.baseURL}` + `${this.api.ParameterList}` + code +`${this.api.ParameterGroupID}` + groupID;
    return this.http.get<ParameterList[]>(url).pipe();
  }

  getAnnualStockTicker1(stockTicker: any): Observable<AnnualStockTicker[]>{
    var url = `${this.api.baseURL}` +  `${this.api.AnnualReports}` + stockTicker + `${this.api.Latest5Year}`;
    return this.http.get<AnnualStockTicker[]>(url).pipe();
  }

  getAnnualParameterValue(stockTicker:any, fiscalYear:any,parameterGroupID: any): Observable<AnnualParameterValue[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnnualReports}` + stockTicker + fiscalYear + `${this.api.data}` + `${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<AnnualParameterValue[]>(url).pipe();
  }

  getAnnualTTM(stockTicker:any, parameterGroupID: any): Observable<AnnualTTM[]>{
    var url = `${this.api.baseURL}` + `${this.api.TTM}` + stockTicker + `${this.api.data}` + `${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<AnnualTTM[]>(url).pipe();
  } 

  getLastPrice(stockTicker:any, parameterGroupID:any): Observable<LastPrice[]>{
    var url =  `${this.api.baseURL}` + `${this.api.LatestPrice}` + stockTicker + `${this.api.data}` + `${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<LastPrice[]>(url).pipe();
  }

  getQuarterStockTicker1(stockTicker:any): Observable<QuarterlyStockTicker[]>{
    var url = `${this.api.baseURL}` + `${this.api.QuarterlyReports}` + stockTicker + `${this.api.Latest12Quarter}`;
    return this.http.get<QuarterlyStockTicker[]>(url).pipe();
  }

  getQuarterParameterValue(stockTicker:any, fiscalYear:any,parameterGroupID: any): Observable<QuarterParameterValue[]>{
    var url = `${this.api.baseURL}` + `${this.api.QuarterlyReports}` + stockTicker + fiscalYear + `${this.api.data}` + `${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<QuarterParameterValue[]>(url).pipe();
  }

//

  getAnnualDetailsTable(): (AnnualDetailsInterface|GroupParameter)[]{
    return SampleAnnualDetails;
  }

  getQuarterDetailsTable(): (QuarterDetailsInterface|GroupParameter)[]{
    return SampleQuarterDetails;
  }

  getAnnualDisplayedColumns(): String[]{
    return ['parameter', 'units','Annual1','Annual2','Annual3','Annual4','Annual5','TTM','LastPrice'];
  }

  getAnnualParameterGroups(): AnnualDetailsParameterGroups[]{
    return SampleAnnualParameterGroups;
  }
  getAnnualParameterValues(): AnnualDetailsParameterValues[]{
    return SampleAnnualParameterValues;
  }
  
  getAnnualStockTicker(): String[]{
    return ['2013 31-Dec','2014 31-Dec','2015 31-Dec','2016 31-Dec','2017 31-Dec'];
  }
 
  getAnnualDataSource(){ 
    var group = this.getAnnualParameterGroups();
    var value = this.getAnnualParameterValues();
    var res = group.map(function(o, i) {
      return {
        parameter: o.parameter,
        units: o.units,
        Annual1: value[i].Annual1,
        Annual2: value[i].Annual2,
        Annual3: value[i].Annual3,
        Annual4: value[i].Annual4,
        Annual5: value[i].Annual5,
        TTM: value[i].TTM,
        LastPrice: value[i].LastPrice,
      }
    })
  return res;
  };

  getQuarterDisplayedColumns(): String[]{
    return  ['parameter', 'units','Quarter1','Quarter2','Quarter3','Quarter4','Quarter5','Quarter6','Quarter7','Quarter8','Quarter9','Quarter10','Quarter11','Quarter12','TTM','LastPrice'];
  }

  getQuarterParameterGroups(): QuarterDetailsParameterGroups[]{
    return SampleQuarterParameterGroups;
  }
  getQuarterParameterValues(): QuarterDetailsParameterValues[]{
    return SampleQuarterParameterValues;
  }

  getQuarterStockTicker(): String[]{
    return ['2015 31-Dec Q4','2016 31-Mar Q1','2016 30-Jun Q2','2016 30-Sep Q3','2016 31-Dec Q4','2017 31-Mar Q1','2017 30-Jun Q2','2017 30-Sep Q3','2017 31-Dec Q4','2018 31-Mar Q1','2018 30-Jun Q2','2018 30-Sep Q3'];
  }
  
  getQuarterDataSource(){
    var group = this.getQuarterParameterGroups();
    var value = this.getQuarterParameterValues();
    var res = group.map(function(o, i) {
      return {
        parameter: o.parameter,
        units: o.units,
        Quarter1: value[i].Quarter1,
        Quarter2: value[i].Quarter2,
        Quarter3: value[i].Quarter3,
        Quarter4: value[i].Quarter4,
        Quarter5: value[i].Quarter5,
        Quarter6: value[i].Quarter6,
        Quarter7: value[i].Quarter7,
        Quarter8: value[i].Quarter8,
        Quarter9: value[i].Quarter9,
        Quarter10: value[i].Quarter10,
        Quarter11: value[i].Quarter11,
        Quarter12: value[i].Quarter12,
        TTM: value[i].TTM,
        LastPrice: value[i].LastPrice,
      }
    })
  return res;
  };
}
