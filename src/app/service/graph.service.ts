import { Injectable } from '@angular/core';
import { DialogParameter } from '../interface/graph';
import { AnnualDetailsInterface, QuarterDetailsInterface } from '../interface/details';
import { SampleParameter } from '../mock-graph';
import { SampleAnnualDetails,SampleQuarterDetails, SampleAnnualDetailsTable, SampleQuarterDetailsTable } from '../mock-graph';
import { AnnualParameterValue,QuarterParameterValue,AnnualStockTicker,QuarterlyStockTicker, AnnualTTM, ParameterList } from '../interface/graph';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { apiConfig } from '../constant';


@Injectable({
    providedIn: 'root',
  })

export class GraphService{
  constructor(
    private api: apiConfig,
    private http: HttpClient,
  ){} 
  initGraph(chart,chartdata){
      chart = new Chart('chart', {
          type: 'bar',
          data: chartdata,
          options: {
            tooltips: {
              mode: 'label',
            },
            elements: {
              line: {
                tension: 0,
                fill: false
              }
          },
            scales: {
                xAxes:[{
                  gridLines:{
                    display:false,
                  },            
                }],
                yAxes: [{
                  id: 'yaxis_1',
                  display: false,
                  gridLines:{
                    display:false,
                    drawBorder: false,
                  },ticks: {
                    beginAtZero: true,  
                },
                },{
                    id: 'yaxis_2',
                    display: false,
                    gridLines:{
                      display:false,
                      drawBorder: false,
                },ticks: {
                  beginAtZero: true,  
              },
              },{
                  id: 'yaxis_3',
                  display: false,
                  gridLines:{
                    display:false,
                    drawBorder: false,
                  },
                  ticks: {
                      beginAtZero: true,  
                  },
                },{
                  id: 'yaxis_4',
                  display: false,
                  gridLines:{
                    display:false,
                    drawBorder: false,
                  },
                  ticks: {
                      beginAtZero: true,  
                  },
                }],
              },
          },
        });
        return chart;
    }

    getAnnualDetailsTable(): AnnualDetailsInterface[]{
      return SampleAnnualDetailsTable;
    }

    getQuarterDetailsTable(): QuarterDetailsInterface[]{
      return SampleQuarterDetailsTable;
    }
    
    getAnnualDisplayedColumns(): String[]{
      return ['parameter', 'units','Annual1','Annual2','Annual3','Annual4','Annual5','TTM','LastPrice'];
    }

    getQuarterDisplayedColumns(): String[]{
      return ['parameter', 'units','Quarter1','Quarter2','Quarter3','Quarter4','Quarter5','Quarter6','Quarter7','Quarter8','Quarter9','Quarter10','Quarter11','Quarter12','TTM','LastPrice'];
    }
  
    getDialogParameter(): DialogParameter[]{
        return SampleParameter;
    }

    addAnnualTableRow(result,dataSource){
      let target = SampleAnnualDetailsTable.filter(x => x.parameter === result);
      dataSource.data.push(target[0]);
      dataSource.filter = "";
    }

    addQuarterTableRow(result,dataSource){
      let target = SampleQuarterDetailsTable.filter(x => x.parameter === result);
      dataSource.data.push(target[0]);
      dataSource.filter = "";
    }

    addAnnualChart(result){
        let target = SampleAnnualDetails.filter(x => x.parameter === result);
        let chart_type = this.getChartType(target[0].units);
        let color = this.randomColorGenerator();
        let newdatasets = {
          type: chart_type[0],
          label: target[0].parameter,
          data: [target[0].Annual1,target[0].Annual2,target[0].Annual3,target[0].Annual4,target[0].Annual5,target[0].TTM],
          backgroundColor: color,
          borderColor: color,
          borderWidth: 3,
          yAxisID: chart_type[1],
        }
        return(newdatasets);
      }

  addQuarterChart(result){
    let target = SampleQuarterDetails.filter(x => x.parameter === result);
        let chart_type = this.getChartType(target[0].units);
        let color = this.randomColorGenerator();
        let newdatasets = {
          type: chart_type[0],
          label: target[0].parameter,
          data: [target[0].Quarter1,target[0].Quarter2,target[0].Quarter3,target[0].Quarter4,target[0].Quarter5,target[0].Quarter6,target[0].Quarter7,target[0].Quarter8,target[0].Quarter9,target[0].Quarter10,target[0].Quarter11,target[0].Quarter12,target[0].TTM,,target[0].LastPrice],
          backgroundColor: color,
          borderColor: color,
          borderWidth: 3,
          yAxisID: chart_type[1],
        }
        return(newdatasets);
    }
    
  randomColorGenerator = function () { 
      return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
  };
  getChartType(unit){
    switch(unit){
      case "RM'000":
        return ['bar','yaxis_1'];
      case "RM":
        return ['bar','yaxis_2'];
      case "cents":
        return ['bar','yaxis_3'];
      case "%":
        return ['line','yaxis_4'];
      case "unit":
        return ['bar','yaxis_5'];
    }
  }

  getQuarterChartData(table){
    return {
    labels: ['Q4-15', 'Q1-16', 'Q2-16', 'Q3-16', 'Q4-16','Q1-17','Q2-17','Q3-17','Q4-17','Q1-18','Q2-18','Q3-18','TTM'],
    datasets: [{
        label: 'Revenue',
        data: [19256,28599,38848,40684,43476,47572,54811,83302,98504,99383,102296,108333,408516],
        backgroundColor: 'rgba(99, 255, 132, 0.5)',
        borderWidth: 1,
        yAxisID: this.getChartType(table[0].units)[1],
    }],
  }}

  getAnnualChartData(table){
    return {
    labels: ['2013', '2014', '2015', '2016', '2017','TTM'],
    datasets: [{
        label: 'Revenue',
        data: [67144,81047,83604,151938,284190,408516],
        backgroundColor: 'rgba(99, 255, 132, 0.5)',
        borderWidth: 1,
        yAxisID: this.getChartType(table[0].units)[1],
    }],
  }}
f
  getAnnualStockTicker(stockTicker: any): Observable<AnnualStockTicker[]>{
    var url = `${this.api.baseURL}` +  `${this.api.AnnualReports}` + stockTicker + `${this.api.Latest5Year}`;
    return this.http.get<AnnualStockTicker[]>(url).pipe();
  }

  getQuarterStockTicker(stockTicker:any): Observable<QuarterlyStockTicker[]>{
    var url = `${this.api.baseURL}` + `${this.api.QuarterlyReports}` + stockTicker + `${this.api.Latest12Quarter}`;
    return this.http.get<QuarterlyStockTicker[]>(url).pipe();
  }

  getParameterList(code:any, groupID: any): Observable<ParameterList[]>{
    var url = `${this.api.baseURL}` + `${this.api.ParameterList}` + code +`${this.api.ParameterGroupID}` + groupID;
    return this.http.get<ParameterList[]>(url).pipe();
  }

  getAnnualParameterValue(stockTicker:any, parameterCode:any,): Observable<AnnualParameterValue[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnnualReports}` + stockTicker +`${this.api.ParameterValue}`+ parameterCode + `${this.api.AndLatest5Year}`;
    return this.http.get<AnnualParameterValue[]>(url).pipe();
  }

  getQuarterParameterValue(stockTicker:any, parameterCode:any,): Observable<QuarterParameterValue[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnnualReports}` + stockTicker +`${this.api.ParameterValue}`+ parameterCode + `${this.api.AndLatest12Quarter}`;
    return this.http.get<QuarterParameterValue[]>(url).pipe();
  }

  getAnnualTTM(stockTicker:any, parameterGroupID: any): Observable<AnnualTTM[]>{
    var url = `${this.api.baseURL}` + `${this.api.TTM}` + stockTicker + `${this.api.data}` + `${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<AnnualTTM[]>(url).pipe();
  } 
}

