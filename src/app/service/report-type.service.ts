import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ReportTypeService{
    private toggle: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
    constructor() { }

    getToggle(): BehaviorSubject<boolean>{
        return this.toggle;
    }
}