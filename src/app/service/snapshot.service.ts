import { Injectable } from '@angular/core';
import { SampleAnnualTable1, SampleAnnualTable2, SampleAnnualTable3, SampleAnnualTable4, SampleAnnualTable5, SampleAnnualTable6, SampleAnnualTable7,SampleAnnualTable8, SampleQuarterTable1, SampleQuarterTable2, SampleQuarterTable3, SampleQuarterTable4, SampleQuarterTable5, SampleQuarterTable6, SampleQuarterTable7, SampleQuarterTable8} from '../mock-snapshot';
import { SnapshotTableParameter, ParameterGroup, ParameterList, AnnualLatestYear, ParameterValue, AnnualChartData, QuarterChartData, QuarterLatestYear } from '../interface/snapshot';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { apiConfig } from '../constant';

@Injectable({
  providedIn: 'root'
})

export class SnapshotService {
  constructor(
    private api: apiConfig,
    private http: HttpClient,
  ) {}

  getParameterGroup(code: any): Observable<ParameterGroup[]>{
    var url = `${this.api.baseURL}` + `${this.api.ParameterGroup}` + code;
    return this.http.get<ParameterGroup[]>(url).pipe(
      catchError((e: any) => Observable.throw(this.errorHandler(e)))
    );
  }
    
  errorHandler(error: any): void {
    console.log(error)
  }

  getParameterList(code:any, groupID: any): Observable<ParameterList[]>{
    var url = `${this.api.baseURL}` + `${this.api.ParameterList}` + code +`${this.api.ParameterGroupID}` + groupID;
    return this.http.get<ParameterList[]>(url).pipe();
  }

  getAnnualLatestYear(stockTicker:any): Observable<AnnualLatestYear[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnnualReports}` + stockTicker + `${this.api.Latest1Year}`;
    return this.http.get<AnnualLatestYear[]>(url).pipe();
  }

  getAnnualParameterValue(stockTicker: any, fiscalYear: any,parameterGroupID: any):Observable<ParameterValue[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnnualReports}` + stockTicker + '/' + fiscalYear + '/' + `${this.api.data}` + `${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<ParameterValue[]>(url).pipe();
  }

  getAnnualChartData(stockTicker: any, parameterCode: any):Observable<AnnualChartData[]>{
    var url = `${this.api.baseURL}` + `${this.api.AnnualReports}` + stockTicker + '/' + `${this.api.ParameterValue}` + parameterCode + `${this.api.AndLatest5Year}`;
    return this.http.get<AnnualChartData[]>(url).pipe();
  }

  getQuarterlyLatestYear(stockTicker:any): Observable<QuarterLatestYear[]>{
    var url = `${this.api.baseURL}` + `${this.api.QuarterlyReports}` + stockTicker + `${this.api.Latest1Quarter}`;
    return this.http.get<QuarterLatestYear[]>(url).pipe();
  }

  getQuarterlyParameterValue(stockTicker: any, fiscalYear: any, quarterID: any,parameterGroupID: any):Observable<ParameterValue[]>{
    var url = `${this.api.baseURL}` + `${this.api.QuarterlyReports}` + stockTicker + '/' + fiscalYear + '/' + quarterID + `${this.api.data}` + `${this.api.ParameterGroupID}` + parameterGroupID;
    return this.http.get<ParameterValue[]>(url).pipe();
  }

  getQuarterlyChartData(stockTicker: any, parameterCode: any):Observable<QuarterChartData[]>{
    var url = `${this.api.baseURL}` + `${this.api.QuarterlyReports}` + stockTicker + `${this.api.ParameterValue}` + parameterCode + `${this.api.AndLatest5Quarter}`;
    return this.http.get<QuarterChartData[]>(url).pipe();
  }


  getAnnualTable(mode): SnapshotTableParameter[]{
    switch(mode){
      case "annualtable1":{
        return SampleAnnualTable1;
      }
      case "annualtable2":{
        return SampleAnnualTable2;
      }
      case "annualtable3":{
        return SampleAnnualTable3;
      }
      case "annualtable4":{
        return SampleAnnualTable4;
      }
      case "annualtable5":{
        return SampleAnnualTable5;
      }
      case "annualtable6":{
        return SampleAnnualTable6;
      }
      case "annualtable7":{
        return SampleAnnualTable7;
      }
      case "annualtable8":{
        return SampleAnnualTable8;
      }
    }
  }

  getQuarterTable(mode): SnapshotTableParameter[]{
    switch(mode){
      case "quartertable1":{
        return SampleQuarterTable1;
      }
      case "quartertable2":{
        return SampleQuarterTable2;
      }
      case "quartertable3":{
        return SampleQuarterTable3;
      }
      case "quartertable4":{
        return SampleQuarterTable4;
      }
      case "quartertable5":{
        return SampleQuarterTable5;
      }
      case "quartertable6":{
        return SampleQuarterTable6;
      }
      case "quartertable7":{
        return SampleQuarterTable7;
      }
      case "quartertable8":{
        return SampleQuarterTable8;
      }
    }
  }

  getTableTitle(mode): string{
    switch(mode){
      case "annualtable1" :
      case "quartertable1":{
        return "Income Statement";
      }
      case "annualtable2" :
      case "quartertable2":{
        return "Cash Flow Statement";
      }
      case "annualtable3" :
      case "quartertable3":{
        return "Balance Sheet Statement";
      }
      case "annualtable4" :
      case "quartertable4":{
        return "Per share Ratio";
      }
      case "annualtable5" :
      case "quartertable5":{
        return "Growth, Margin, Valuation";
      }
      case "annualtable6" :
      case "quartertable6":{
        return "Financial Ratios";
      }
      case "annualtable7" :
      case "quartertable7":{
        return "Liquidity/Debt Ratio";
      }
      case "annualtable8" :
      case "quartertable8":{
        return "Analyzer Scores";
      }
    }
} 
  getChartData1(mode): number[]{
    switch(mode){
      case "annualbarChart1":{
        return [2852,6043,12290,29585,39172];
      }  
      case "annualbarChart2":{
        return [5275,11962,7886,17466,34097];
      }  
      case "annualbarChart3":{
        return [84652,89843,96555,143471,356249];
      }  
      case "annualbarChart4":{
        return [1.79,3.40,8.97,18.76,11.36];
      }  
      case "quarterbarChart1":{
        return [8401,11960,15054,22930,26717];
      }  
      case "quarterbarChart2":{
        return  [-2614,24098,7695,17019,66981];
      }  
      case "quarterbarChart3":{
        return [287206,356249,550323,605259,640679];
      }  
      case "quarterbarChart4":{
        return [5.30,3.26,2.28,4.41,5.40];
      }  
    }
  }

  getChartLabel(mode){
    switch(mode){
      case "annualbarChart1":
      case "quarterbarChart1":{
        return ['Revenue','Net Profit'];
      }
      case "annualbarChart2":
      case "quarterbarChart2":{
            return ['Net Operation','Net Cash Flow'];
          }
      case "annualbarChart3":
      case "quarterbarChart3":{
        return ['Total Asset','Total Stakeholders Equity'];
      }
      case "annualbarChart4":
      case "quarterbarChart4":{
        return ['Earning Per Share','Dividend Per Share'];
      }
    }
  }

  getChartData2(mode): number[]{
    switch(mode){
      case "annualbarChart1":{
        return [67344,81047,83604,151938,284190];
      }  
      case "annualbarChart2":{
        return [5275,11962,7886,17466,34097];
      }  
      case "annualbarChart3":{
        return [56879,62942,77581,112174,200347];
      }  
      case "annualbarChart4":{
        return [1,2,4,6,8];
      }  
      case "quarterbarChart1":{
        return [83302 ,98504,99383,102296,108333];
      }  
      case "quarterbarChart2":{
        return  [-2614,24098,7695,17019,66981];
      }  
      case "quarterbarChart3":{
        return [158886,200346,395492,418442,445139];
      }  
      case "quarterbarChart4":{
        return [2,2,2,3,3];
      }  
    }
  }
  getChartLabelString(mode){
    switch(mode){
      case "annualbarChart1":
      case "quarterbarChart1":{
        return 'Income Statement';
      }
      case "annualbarChart2":
      case "quarterbarChart2":{
            return 'Cash Flow Statement';
          }
      case "annualbarChart3":
      case "quarterbarChart3":{
        return 'Balance Sheet';
      }
      case "annualbarChart4":
      case "quarterbarChart4":{
        return 'Per Share Ratio';
      }
    }
  }
  
  getChartTicker(lastEvent): string[]{
    switch(lastEvent){
      case true:{
        return ['2013','2014','2015','2016','2017']
      }
      case false:{
        return ['Q3-17', 'Q4-17', 'Q1-18', 'Q2-18', 'Q3-18']
      }
    }
  }
}
