import { Component, Input, ViewChild, ElementRef,OnChanges} from '@angular/core';
import { SnapshotService } from '../../service/snapshot.service'
import { Chart } from 'chart.js';

@Component({
  selector: 'app-snapshot-chart',
  templateUrl: './snapshot-chart.component.html',
  styleUrls: ['./snapshot-chart.component.scss']
})
export class SnapshotChartComponent implements OnChanges {
  @Input() mode: string
  @Input() lastEvent: boolean;
  @Input() parameterCode: string;
  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;
  public chart: any = null;
  public chartlabel: string[];
  public chartlabelstring: string;
  constructor(
    private SnapshotService: SnapshotService,
  ){ } 

  ngOnChanges(){
    this.SnapshotService.getAnnualChartData(7160,this.parameterCode).subscribe((res)=>{
      console.log(res);
    }) 
    let ctx = this.canvas.nativeElement;
    this.chartlabel = this.SnapshotService.getChartLabel(this.mode);
    this.chartlabelstring = this.SnapshotService.getChartLabelString(this.mode);
    this.chart = new Chart(ctx, {
      type: 'bar',
      data: { 
        labels: this.SnapshotService.getChartTicker(this.lastEvent),
        datasets: [{     
            label: this.SnapshotService.getChartLabel(this.mode)[0],
            data: this.SnapshotService.getChartData1(this.mode),
            backgroundColor: 'rgba(99, 255, 132, 1)',
            borderWidth: 1
        },{
          label: this.SnapshotService.getChartLabel(this.mode)[1],
          data: this.SnapshotService.getChartData2(this.mode),
          backgroundColor: 'rgba(1,1,255,1)',
          borderWidth: 1
        }],
      },
      options: {
        responsive: true, 
        maintainAspectRatio:false,
        legend:{
          position: 'top',
          labels:{
            fontSize: 8,
          }
        },
        scales: {
            xAxes:[{
              gridLines:{
                display:false,
              },
            }],
            yAxes: [{
              gridLines:{
                display:false,
              },
              scaleLabel: {
                display: true,
                fontSize: 10, 
                labelString: this.chartlabelstring,
              },
              ticks: {
                  beginAtZero: true, 
                  callback: function(label, index, labels) {
                    if(label>1000){
                      return label/1000+'k';
                    }else if(label<-999){
                      return label/1000+'k';
                    }else{
                      return label;
                    }
                  } 
              },
            }],
          },
      },
    });
  }
}
