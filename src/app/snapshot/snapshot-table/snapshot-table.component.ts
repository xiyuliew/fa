import { Component, OnInit, Input} from '@angular/core';
import { SnapshotService } from '../../service/snapshot.service';

@Component({
  selector: 'app-snapshot-table',
  templateUrl: './snapshot-table.component.html',
  styleUrls: ['./snapshot-table.component.scss']
})
export class SnapshotTableComponent implements OnInit {
  @Input() mode: string;
  @Input() lastEvent: boolean;
  @Input() id: number;
  displayedColumns = ['parameter', 'value'];
  dataSource = [];
  array = [];
  result: any;
  title: string; 
  date: any;
  constructor(
    private SnapshotService: SnapshotService,
  ) { }
  
  ngOnInit() {
    if(this.lastEvent){
      this.getSnapshotAnnualTable();
    }else{
      this.getSnapshotQuarterTable();
    }
  } 
 
  // getSnapshotAnnualTable(){
  //   this.SnapshotService.getAnnualLatestYear('7160').subscribe((res)=>{
  //     this.result = res;
  //     this.date = new Date(this.result[0].fye_date);

  //     this.SnapshotService.getParameterGroup('DBAN').subscribe((res)=>{
  //       this.result = res;
  //       this.title = this.result[this.id].name;

  //       this.SnapshotService.getParameterList('DBAN',(27+this.id)).subscribe((res)=>{
  //         this.result = res;
  //         for(var i=0;i < this.result.length ;i++){  
  //           this.array.push({parameter: this.result[i].full_name});
  //         }
          
  //         this.SnapshotService.getAnnualParameterValue(7160,2017,(27+this.id)).subscribe((res)=>{
  //           this.result = res; 
  //           for(var i=0;i < this.result.length ;i++){  
  //             this.array[i].value = this.result[i].fmt_value;
  //           }
  //           this.dataSource = this.array;
  //         }) 
  //       })
  //     })
  //   })
  // }

  getSnapshotAnnualTable(){
    this.date = '31-Dec-2017';
    this.title = this.SnapshotService.getTableTitle(this.mode);
    this.dataSource = this.SnapshotService.getAnnualTable(this.mode);
  }


  // getSnapshotQuarterTable(){
  //   this.SnapshotService.getQuarterlyLatestYear('7160').subscribe((res)=>{
  //     this.date = new Date(res[0].quarter_date);

  //     this.SnapshotService.getParameterGroup('DBQR').subscribe((res)=>{
  //       this.title = res[this.id].name;

  //       this.SnapshotService.getParameterList('DBQR',(35+this.id)).subscribe((res)=>{
  //         this.result = res;
  //         for(var i=0;i < this.result.length ;i++){  
  //           this.array.push({parameter: this.result[i].full_name});
  //         }

  //         this.SnapshotService.getQuarterlyParameterValue(7160,2018,3,(35+this.id)).subscribe((res)=>{
  //           this.result = res; 
  //           for(var i=0;i < this.result.length ;i++){  
  //             this.array[i].value = this.result[i].fmt_value;
  //           }
  //           this.dataSource = this.array;
  //         }) 
  //       })
  //     }) 
  //   }) 

  getSnapshotQuarterTable(){
    this.date = '30-Sep-2018 (Q3)'
    this.title = this.SnapshotService.getTableTitle(this.mode);
    this.dataSource = this.SnapshotService.getQuarterTable(this.mode);
  }
}
