import { Component,OnInit} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ReportTypeService } from '../service/report-type.service';

@Component({
  selector: 'app-snapshot',
  templateUrl: './snapshot.component.html',
  styleUrls: ['./snapshot.component.scss'],
})

export class SnapshotComponent implements OnInit{
  toggle$: BehaviorSubject<boolean>;
  lastEvent:boolean = true;
  constructor(
    private ReportTypeService: ReportTypeService, 
    ){
    this.toggle$ = this.ReportTypeService.getToggle();
  };
  
  ngOnInit(){
    this.toggle$.subscribe(value => {
      this.lastEvent = value;
    })
  }
}
